
public class AccountHolder {
	
	public void AccountHolder(String firstName, String middleName, String lastName,
			String ssn, double checkingAccountOpeningBalance, double saveAccountOpeningBalance) {
		
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		checkingAccount = checkingAccountOpeningBalance;
		saveAccount = saveAccountOpeningBalance;
		this.ssn = ssn;
		}
	
	public String getFirstName() {
		return firstName;
	}
	
	
	public String getMiddleName() {
		return middleName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getSsn() {
		return ssn;
	}
	
	public double getCheckingAccount() {
		return checkingAccount;
		}
	
	public double getSavingAccount() {
		return saveAccount;
		}
	
	
	public String toString() {
		
		CheckingAccount checking = new CheckingAccount();
		checking.checkingAccount(getCheckingAccount());
		checking.futureValue(3);
		
		SavingsAccount saving = new SavingsAccount();
		saving.SavingAccount(getSavingAccount());
		saving.futureValue(3);
		
		return ("Name: " + getFirstName() + getMiddleName() + getLastName() + "\n"+ 
				"SSN: " + getSsn() + "\n"+ checking.toString() + "\n" + saving.toString());
	}
	
	private String firstName, middleName,lastName, ssn;
	private double checkingAccount, saveAccount;
	

}
